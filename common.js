//TODO: Have a module destroy

// top-level namespace being assigned an object literal
var app = app || {};

// function for parsing string namespaces and
// automatically generating nested namespaces
//Example : var module = extend(app, "pay.earnings");

function extend(ns, ns_string) {
    "use strict";
    var parts = ns_string.split('.'),
        parent = ns,
        pl, i;
    if (parts[0] == "app") {
        parts = parts.slice(1);
    }
    pl = parts.length;
    for (i = 0; i < pl; i++) {
        //create a property if it doesnt exist
        if (typeof parent[parts[i]] == 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
}

(function() {
    "use strict";

    var Observable;

    Observable = (function() {
        var utils;

        utils = {
            is: function(type, value) {
                return Object.prototype.toString.call(value).match(/\s(\w+)/)[1].toLowerCase() === type;
            },
            isPlainObject: function(value) {
                return !!value && utils.is('object', value);
            },
            toArray: function(value) {
                if (utils.is('array', value)) {
                    return value;
                } else {
                    return [value];
                }
            }
        };

        function Observable(host) {
            var fn, key, _ref;
            if (host === null) {
                host = {};
            }
            host.__observable = {
                lastIds: {},
                events: {},
                ids: []
            };
            _ref = Observable.prototype;
            for (key in _ref) {
                fn = _ref[key];
                host[key] = fn;
            }
            return host;
        }

        Observable.prototype.on = function(topics, fn, once) {
            var id, topic, _base, _base1, _i, _len;
            if (utils.isPlainObject(topics)) {
                once = fn;
                for (topic in topics) {
                    fn = topics[topic];
                    this.on(topic, fn, once);
                }
            } else {
                topics = utils.toArray(topics);
                this.__observable.ids.length = 0;
                for (_i = 0, _len = topics.length; _i < _len; _i++) {
                    topic = topics[_i];
                    (_base = this.__observable.lastIds)[topic] || (_base[topic] = 0);
                    id = "" + topic + ";" + (++this.__observable.lastIds[topic]);
                    if (once) {
                        id += ' once';
                    }
                    this.__observable.ids.push(id);
                    (_base1 = this.__observable.events)[topic] || (_base1[topic] = {});
                    this.__observable.events[topic][id] = fn;
                }
            }
            return this;
        };

        Observable.prototype.once = function(topics, fn) {
            return this.on(topics, fn, true);
        };

        Observable.prototype.off = function(obj) {
            var id, topic, _i, _len, _ref;
            if (obj === void 0) {
                this.__observable.events = {};
            } else {
                _ref = obj.__observable.ids;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    id = _ref[_i];
                    topic = id.substr(0, id.lastIndexOf(';')).split(' ')[0];
                    if (obj.__observable.events[topic]) {
                        delete obj.__observable.events[topic][id];
                    }
                }
            }
            return this;
        };

        Observable.prototype.trigger = function(topic, args) {
            var fn, id, _ref;
            if (args === null) {
                args = [];
            }
            _ref = this.__observable.events[topic];
            for (id in _ref) {
                fn = _ref[id];
                fn.apply(null, args);
                if (id.lastIndexOf(' once') === id.length - 1) {
                    this.off(id);
                }
            }
            return this;
        };

        return Observable;

    })();

    if (typeof define === 'function' && define.amd) {
        define('observable', [], function() {
            return Observable;
        });
    } else if (typeof exports !== 'undefined') {
        module.exports = Observable;
    } else {
        window.Observable = Observable;
    }

}).call(this);


var commonMod = extend(app, "common");

// commonMod.Json = function () {

//     //Load JSON Data
//     var load = function (callBackSuccess, callBackError) {

//         $.ajax({
//             url: "http://localhost/payatworkapi/api/payformprocessing/1",
//             type: "GET",
//             cache: "false",
//         })

//         .done(function (data) {
//                 callBackSuccess(data);
//             }
//         )

//         .fail(function (jqXHR, textStatus) {
//             callBackError(textStatus);
//         });
//     }

//     var retObj = {
//         load: load
//     }

//     return retObj;
// };


commonMod.Json = function() {
    "use strict";

    //Load JSON Data
    var load = function() {
        return $.ajax({
            url: "http://localhost/payatworkapi/api/payformprocessing/1",
            type: "GET",
            cache: "false",
        });
    };

    var retObj = {
        load: load
    };

    return retObj;
};

commonMod.DataSync = function(contextData, navContext) {
    "use strict";

    var me = this;

    me._modelKeys = [];

    me._nav = navContext;

    me.contextData = contextData;

    var addItem = function(itemToAdd, source) {
        me.contextData.push(itemToAdd);
        retObj.trigger('itemAdded', [source, itemToAdd]);
    };

    var removeItem = function(itemToRemove, source) {
        me.contextData.splice(me.contextData.indexOf(itemToRemove), 1);
        retObj.trigger('itemRemoved', [source, itemToRemove]);
    };

    var replaceItemAt = function(itemIndex, itemToUpdateTo, source) {
        me.contextData[itemIndex] = itemToUpdateTo;
        retObj.trigger('itemUpdated', [source, itemToUpdateTo]);
    };

    var updateItemValue = function(item, key, value, source) {
        var updateFrom = me.contextData[me.contextData.indexOf(item)][key];
        me.contextData[me.contextData.indexOf(item)][key] = value;
        retObj.trigger('itemValueUpdated', [source, updateFrom, value]);
    };

    var bind = function(model, bindTo) {
        var currentItem = me._nav.getCurrentRecord();

        for (var key in model.fields) {

            var keyVal = model.fields[key];
            me._modelId = model.id;
            me._modelKeys.push(keyVal);
            me._bindTo = bindTo;

            //var changingItem = $(me._bindTo + " > #" + keyVal);
            //changingItem.val(currentItem[keyVal]);
        }

        populateData();
    };

    var populateData = function() {
        var currentItem = me._nav.getCurrentRecord();
        for (var key in me._modelKeys) {
            var name = me._modelKeys[key];
            var changingItem = $(me._bindTo + " > #" + name);

            changingItem.val(currentItem[name]);
        }
    };


    //TODO: Can we have access to navigation here itself

    var sync = function(source) {
        var currentItem = navContext.getCurrentRecord();

        for (var key in me._modelKeys) {
            var name = me._modelKeys[key];
            var changingItem = $(me._bindTo + " > #" + name);

            currentItem[name] = changingItem.val();
        }

        retObj.trigger('itemUpdated', [source, currentItem]);
    };

    var retObj = {
        addItem: addItem,
        removeItem: removeItem,
        replaceItemAt: replaceItemAt,
        updateItemValue: updateItemValue,
        bind: bind,
        sync: sync,
        refreshBind: populateData
    };

    Observable(retObj);

    return retObj;
};


commonMod.Navigation = function(contextData) {
    "use strict";
    var me = this;

    me._contextData = contextData;
    me._currentPosition = 0;

    me.currentView = "form";

    var getCurrentPosition = function() {
        return me._currentPosition;
    };

    var moveFirst = function() {
        var prevPos = me._currentPosition;
        me._currentPosition = 0;
        if (prevPos != me._currentPosition) _publishMove(getCurrentRecord());
    };

    var moveLast = function() {
        var prevPos = me._currentPosition;
        me._currentPosition = me._contextData.length === 0 ? 0 : me._contextData.length - 1;
        if (prevPos !== me._currentPosition) _publishMove(getCurrentRecord());
    };

    var movePrevious = function() {
        var prevPos = me._currentPosition;
        me._currentPosition = (me._currentPosition === 0) ? 0 : me._currentPosition - 1;
        if (prevPos != me._currentPosition) _publishMove(getCurrentRecord());
    };

    var moveNext = function() {
        var prevPos = me._currentPosition;
        me._currentPosition = (me._currentPosition == me._contextData.length - 1) ? me._contextData.length - 1 : me._currentPosition + 1;
        if (prevPos != me._currentPosition) _publishMove(getCurrentRecord());
    };

    var moveTo = function(pos) {
        var prevPos = me._currentPosition;
        me._currentPosition = (me._contextData.length <= pos) ? me._contextData.length : pos;
        if (prevPos != me._currentPosition) _publishMove(getCurrentRecord());
    };

    var getCurrentRecord = function() {
        return me._contextData[me._currentPosition];
    };

    var switchView = function() {
        //TODO: Practice to use ===
        if (me.currentView === "form")
            me.currentView = "grid";
        else
            me.currentView = "form";

        retObj.trigger("onSwitchView", [me.currentView]);
    };

    var refresh = function() {

        retObj.trigger("onRefresh", [getCurrentRecord()]);
    };

    var _publishMove = function(data) {
        retObj.trigger("onMove", [data]);
    };

    var retObj = {
        getCurrentPosition: getCurrentPosition,
        moveFirst: moveFirst,
        moveLast: moveLast,
        movePrevious: movePrevious,
        moveNext: moveNext,
        moveTo: moveTo,
        getCurrentRecord: getCurrentRecord,
        switchView: switchView,
        refresh: refresh
    };

    Observable(retObj);

    return retObj;

};

var kendoExt = extend(app, "common.kendo.ext");

kendoExt.dataSourceExtensions = {
    updateField: function(e) {
        var ds = this;
        $.each(ds._data, function(idx, record) {
            if (record[e.keyField] == e.keyValue) {
                ds._data[idx][e.updateField] = e.updateValue;
                ds.read(ds._data);
                return false;
            }
        });
    },

    update: function(model, itemUpdated) {
        for (var key in model.fields) {
            var name = model.fields[key];
            this.updateField({
                keyField: model.id,
                keyValue: itemUpdated[model.id],
                updateField: name,
                updateValue: itemUpdated[name]
            });
        }
    }
};

$.extend(true, kendo.data.DataSource.prototype, app.common.kendo.ext.dataSourceExtensions);
